<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//set multi language
Route::group(['middleware' => 'localization', 'prefix' => Session::get('locale')], function(){
	Route::get('/','Home\HomeController@index');
	Route::get('/language/{locale}', 'LangController@setLang');
	Route::get('post/detail/{id}','Home\HomeController@detail');
	Route::get('/home','Home\HomeController@index');
	Auth::routes();
	Route::group([ 'middleware' => 'auth'], function(){
		Route::get('category', function(){
			return "ok";
		});
	});
});

// Route::get('post/detail/{id}','Home\HomeController@detail');
// Auth::routes();

// Route::group([ 'middleware' => 'auth'], function(){
// 	Route::get('category', function(){
// 		return "ok";
// 	});
// });

// Route::get('admin/login',function(){
// 	return view('admin.auth.login');
// });
// Route::post('admin/login','AdminAuth\LoginController@login');
// Route::group(array('prefix'=>'admin', 'middleware'=>'AdminAuth'),function(){

// 	Route::get('home',function(){
// 		return view('admin.home');
// 	});
// });

Route::get('admin/login', 'AdminAuth\LoginController@showLoginForm');

Route::post('admin/login', 'AdminAuth\LoginController@login');

Route::post('admin/logout', 'AdminAuth\LoginController@logout');

Route::get('admin/home','AdminController@index');

Route::group(['prefix' => 'admin', 'middleware' => 'AdminAuth'], function(){
	Route::get('/','AdminController@index');
	Route::get('user', 'Admin\UserController@list_user');

	Route::get('user/add',function(){
		return view('admin.user.add_user');
	});

	Route::post('user/add','Admin\UserController@add_user');


	Route::get('user/edit/{id}','Admin\UserController@edit_user');
	Route::post('user/edit/{id}', 'Admin\UserController@do_edit_user');

	Route::get('user/delete/{id}','Admin\UserController@delete_user');

	// insert edit delete post
	Route::get('post','Admin\PostController@list_post');

	Route::get('post/add','Admin\PostController@add');

	Route::post('post/add','Admin\PostController@do_add');

	Route::get('post/delete/{id}','Admin\PostController@delete');

	Route::get('post/edit/{id}','Admin\PostController@edit');

	Route::post('post/edit/{id}','Admin\PostController@do_edit');

	//insert delete comment
	Route::get('comment','Admin\CommentController@list_comment');

	Route::get('comment/delete/{id}','Admin\CommentController@delete');
});