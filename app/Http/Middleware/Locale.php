<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Lang;
class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!SESSION::has('locale')){
            SESSION::put('locale', config('app.locale'));
        }
        
        Lang::setLocale(SESSION::get('locale'));
        return $next($request);
    }
}
