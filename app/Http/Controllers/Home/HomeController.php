<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class HomeController extends Controller
{
    //
    public function index()
    {
    	# code...
    	$data_left = DB::table('tbl_post')->select('id', 'image', 'title', 'description', 'created_at')->paginate(6);
    	$data_right = DB::table('tbl_post')->select('id', 'image', 'title')->orderBy('id', 'desc')->limit(5)->get();
    	return view('home-page',['data_right' => $data_right, 'data_left' => $data_left]);
    }

    //display content news
    public function detail($id)
    {
    	# code...
    	$data_right = DB::table('tbl_post')->select('id', 'image', 'title')->orderBy('id', 'desc')->limit(5)->get();
    	$data_left = DB::table('tbl_post')->where('id',$id)->first();
        $arr_comment = DB::table('tbl_comment')->where('post_id',$id)->join('users', 'users.name', '=', 'tbl_comment.author')->select('tbl_comment.*', 'users.name', 'users.email')->orderBy('created_at')->get();

    	return view('detail',['data_right' => $data_right, 'data_left' => $data_left, 'arr_comment' => $arr_comment]);
    }
}
