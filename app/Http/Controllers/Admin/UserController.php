<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Validator;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    //
    public function list_user()
    {
    	# code...
    	$data = DB::table('users')->paginate(2);
    	return view('admin.user.list_user',['data' => $data]);
    }

    //function add user
    public function add_user(Request $request)
    {
    	# code...
    	$validator = Validator::make($request->all(),[
    		'name' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
    	]);

    	if ($validator->fails()){
    		return redirect('admin/user/add')
    				->withErrors($validator)
    				->withInput()
    		;
    	}
    	else{
    		$user = new User;
    		$user->name = $request->name;
    		$user->email = $request->email;
    		$user->password = Hash::make($request->password);
    		$user->remember_token = $request->_token;
    		$user->save();
    		return redirect('admin/user');
    	}
    }

    //function display edit user
    public function edit_user($id)
    {
    	# code...
    	$data = DB::table('users')->where('id', '=', $id)->first();
    	return view('admin.user.edit_user',['data' => $data]);
    }

    //function do edit user
    public function do_edit_user($id, Request $request)
    {
    	# code...
    	$user = User::where('id', $id)->first();
    	$name = $user->name;
    	$email = $user->email;
    	$user->name="";
    	$user->email="";
    	$user->save();
    	$validator = Validator::make($request->all(),[
    		'name' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'min:6|confirmed',
    	]);

    	if ($validator->fails()){
    		$user->name = $name;
    		$user->email = $email;
    		$user->save();
    		return redirect('admin/user/edit/' . $id)
    				->withErrors($validator)
    				->withInput()
    		;
    	}
    	else{
    		$user->name = $request->name;
    		$user->email = $request->email;
    		$user->password = isset($request->password) ? Hash::make($request->password) : $user->password;
    		$user->remember_token = $request->_token;
    		$user->save();
    		return redirect('admin/user');
    	}
    }

    //delete user
    public function delete_user($id)
    {
    	# code...
    	DB::table('users')->where('id', '=', $id)->delete();
    	return redirect('admin/user');
    }
}
