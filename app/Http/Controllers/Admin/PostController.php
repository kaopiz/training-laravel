<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use File;

class PostController extends Controller
{
    //display list posts
    public function list_post()
    {
    	# code...
    	$list_post = DB::table('tbl_post')->select('id', 'category', 'image', 'title', 'status', 'author')->paginate(2);
    	return view('admin.post.list_post',['data' => $list_post]);
    }

    //display form add post
    public function add()
    {
    	# code...
    	return view('admin.post.add_post');
    }

    public function do_add(Request $request){
    	//validation request
    	$validator = Validator::make($request->all(),[
    		'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
    		'title' => 'required|max:200|min:3',
    		'author' => 'required|max:200|min:3',
    	]);

    	if ($validator->fails()){
    		return redirect('admin/post/add')
    				->withErrors($validator)
    				->withInput()
    		;
    	}
    	else{
			$create = date("Y-m-d H:i:s", time()); 

    		//move file image
    		$imageName = '';
    		if ( $request->hasFile('image') ){

    			//Display File Name
    		    $imageName = time().'.'.$request->image->getClientOriginalExtension();
    		    $request->image->move('images/post', $imageName);
			}
			$img = 'images/post/' . $imageName;
			$status = $request->status=="true" ? 1:0;

    		//insert database
    		DB::table('tbl_post')->insert([ 
                'category'=> $request->category,
                'image' => $img,
                'title' => $request->title,
                'description' => $request->description,                            
                'content' => $request->content,
                'status' => $status,
                'author' => $request->author,
                'created_at' =>  $create 
            ]);
    		
    		return redirect('admin/post/');
    	}
    }

    //delete record
    public function delete($id)
    {
    	# code...
    	$arr = DB::table('tbl_post')->where('id' ,$id)->first();
    	File::Delete($arr->image);
    	DB::table('tbl_post')->where('id', $id)->delete();
    	return redirect('admin/post');
    }


    //form edit record
    public function edit($id)
    {
    	# code...
    	$data = DB::table('tbl_post')->where("id", $id)->first();
    	return view('admin.post.edit_post',['data' => $data]);
    }

    //do edit record
    public function do_edit($id, Request $request)
    {
    	# code...
    	//validation request
    	$validator = Validator::make($request->all(),[
    		'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
    		'title' => 'required|max:200|min:3',
    		'author' => 'required|max:200|min:3',
    	]);

    	if ($validator->fails()){
    		return redirect('admin/post/edit/'.$id)
    				->withErrors($validator)
    				->withInput();
    	}
    	else{
			$update = date("Y-m-d H:i:s", time());
			$status = $request->status=="true" ? 1 : 0; 
    		if ($request->hasFile('image')){
    			//move file
    			$imageName = time().'.'.$request->image->getClientOriginalExtension();
    		    $request->image->move('images/post', $imageName);
    		    $img = 'images/post/' . $imageName;
    		    DB::table('tbl_post')->where('id', $id)->update([ 
                    'category' => $request->category,
                    'image' => $img,
                    'title' => $request->title,
                    'description' => $request->description,
                    'content' => $request->content,
                    'status' => $status,
                    'author' => $request->author,
                    'updated_at' => $update 
                ]);
    		}
    		else{
    			DB::table('tbl_post')->where('id', $id)->update([
                    'category' => $request->category,
                    'title' => $request->title,
                    'description' => $request->description,
                    'content' => $request->content,
                    'status' => $status,
                    'author' => $request->author,
                    'updated_at'=>$update
                ]);
    		}
    		return redirect('admin/post');
    	}
    }
}
