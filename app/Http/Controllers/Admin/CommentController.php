<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
class CommentController extends Controller
{
    //
    public function list_comment()
    {
    	# code...
    	$data = DB::table('tbl_comment')->paginate(2);
    	return view('admin.comment.list_comment', ['data' => $data]);
    }

    public function delete($id)
    {
    	# code...
    	DB::table('tbl_comment')->where('id', $id)->delete();
    	return redirect('admin/comment');
    }
}
