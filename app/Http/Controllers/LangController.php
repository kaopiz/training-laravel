<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
class LangController extends Controller
{
    //
    public function setLang($locale)
    {
    	# code...
    	SESSION::put('locale', $locale);
    	return redirect()->back();
    }
}
