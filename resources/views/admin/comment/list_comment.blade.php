@extends('admin.layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-primary" style="margin-top: 15px;">
				<div class="panel-heading">List comment</div>
				<div class="panel-body">
					<table class="table table-hover table-bordered">
						<tr>
							<th style="">STT</th>
							<th style="">User</th>
							<th>Post</th>
							<th>Content</th>
							<th>Create</th>
							<th></th>
						</tr>
						<?php
							$i = 0;
						?>
						@foreach($data as $row)
						<tr>
							<td><?php echo ++$i ?></td>
							<td>
								<?php
									$email = DB::table('users')->select('email')->where('name', $row->author)->get();

								?>
								{{ $row->author }} ( {{ $email[0]->email }} ) 
							</td>
							<td>
								<?php
									$post = DB::table('tbl_post')->select('title')->where('id', $row->post_id)->get();

								?>
								{{ $post[0]->title }} 
							</td>
							<td>{{ $row->content }}</td>
							<td>{{ $row->created_at }}</td>
							<td>
								<a href="{{ asset('admin/comment/delete') }}/{{$row->id}}" class="btn btn-danger" onclick="return window.confirm('Are you sure ?');">Delete</a>
							</td>
						</tr>
						
						@endforeach
					</table>		
				</div>
				{{ $data->links() }}
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	.pagination{
		margin-top: -20px;
		margin-left: 15px;
	}
</style>
@endsection