@extends('admin.layouts.app')

@section('content')
<script language="javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit post</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/post/edit') }}/{{ $data->id }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <!-- image -->
                        <div>
                            <label class="col-md-2 control-label">Image</label>

                            <div class="col-md-10">
                                <input type="file" class="form-control" name="image" value="{{ old('image') }}" style="border: 0" >

                                @if ($errors->has('file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!--  end image -->

                        <!-- category -->
                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">Category</label>

                            <div class="col-md-10">
                                <select name="category">
                                    <option value="Xã hội" {{ $data->category=='Xã hội' ? 'selected':'' }}>Xã hội</option>
                                    <option value="Thế giới" {{ $data->category=='Thế giới' ? 'selected':'' }}>Thế giới</option>
                                    <option value="Kinh tế" {{ $data->category=='Kinh tế' ? 'selected':'' }}>Kinh tế</option>
                                    <option value="Giáo dục" {{ $data->category=='Giáo dục' ? 'selected':'' }}>Giáo dục</option>
                                    <option value="Thể thao" {{ $data->category=='Thể thao' ? 'selected':'' }}>Thể thao</option>
                                    <option value="Khoa học - công nghệ" {{ $data->category=='Khoa học - công nghệ' ? 'selected':'' }}>Khoa học - công nghệ</option>
                                </select>
                            </div>
                        </div>
                        <!-- end category -->

                        <!-- title -->
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">Title</label>

                            <div class="col-md-10">
                                <input  type="title" class="form-control" name="title" value="{{ old('title') ? old('title') : $data->title }}" required>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- end tittle -->

                        <!-- description -->
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-2 control-label">Description</label>

                            <div class="col-md-10">
                                <input style="width: 100%" class="form-control" name="description" value="{{ old('description') ? old('description') : $data->description }}" required>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- end description -->

                        <!-- content -->
                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">Content</label>

                            <div class="col-md-10">
                                <textarea class="ckeditor" name="content" required>
                                    <?php 
                                        $content = old('content');
                                    ?>
                                    {{ isset($content) ? $content:$data->content }}
                                </textarea>
                                
                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- end content -->

                        <!-- author -->
                        <div class="form-group{{ $errors->has('author') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">Author</label>

                            <div class="col-md-10">
                                <input  type="text" class="form-control" name="author" value="{{ old('author') ? old('author') : $data->author  }}" required>

                                @if ($errors->has('author'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('author') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- end author -->

                        <!-- status -->
                        <div>
                            <label class="col-md-2 control-label">Status</label>

                            <div class="col-md-10">
                                <select name="status">
                                    <option value="false" {{ $data->status==0 ? "selected" : "" }}>0</option>
                                    <option value="true" {{ $data->status==1 ? "selected" : "" }}>1</option>
                                </select>
                            </div>
                        </div>
                        <!-- end status -->

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
