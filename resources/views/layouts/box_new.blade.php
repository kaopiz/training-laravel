
<div class="row box-left">
    <div class="col-md-4 image">
        <a href="{{ asset('post/detail/'.$arr->id) }}"><img src="{{ asset($arr->image) }}"></a>
    </div>
    <div class="col-md-8 news">
        <div class="title">
            <a href="{{ asset('post/detail/'.$arr->id) }}">{{ $arr->title }}</a>    
        </div>
        <div class="description">
            {{ $arr->description }}
        </div>
        <div class="time">
            <span class="glyphicon glyphicon-time"></span> {{ $arr->created_at }}
        </div>
    </div>
</div>
<hr>
