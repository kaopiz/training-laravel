
<div class="comment">
@if (Auth::guest())
	<div class="guest">
		<a class="btn btn-primary" href="{{ asset('login') }}">{{ Lang::get('label.login') }}</a> hoặc
		<a class="btn btn-primary" href="{{ asset('register') }}">{{ Lang::get('label.register') }}</a> đăng ký để được bình luận 
	</div>
@endif
	<div class="user">
		<div class="list-comment">
			@foreach($arr as $row)
			<!-- cmt 1 -->
			<div class="content">
				<div class="row">
					<div class="col-md-1 image">
						<img src="{{ asset('images/home/ichat.gif') }}">
					</div>
					<div class="col-md-11">
						<div class="name">{{ $row->name }} ( {{ $row->email }} ) </div>
						<div><span class="glyphicon glyphicon-time"></span> {{ $row->created_at }}</div>
						<div class="cmt">{{ $row->content }}</div>
					</div>
				</div>
			</div>
			<!-- end cmt 1 -->
			@endforeach
		@if (Auth::check())
			<div class="box">
				<form>
					<label class="name">{{ Auth::user()->name }} ({{ Auth::user()->email }})</label>
					<textarea name="comment" style="width: 100%" rows="5" placeholder="Bạn nhập bình luận tại đây"></textarea>
					<input type="submit" name="" value="Bình luận" class="btn btn-primary">
				</form>
			</div>
		@endif
		</div>
	</div>
</div>

