<div class="content">
	<div class="title">
		{{ $arr->title }}
	</div>
	<div class="time">
        <span class="glyphicon glyphicon-time"></span> {{ $arr->created_at }}
    </div>
    <hr>
    <div class="image">
        <a href=""><img src="{{ asset($arr->image) }}"></a>
    </div>
    <div class="content">
    	<?php echo $arr->content ?>
    </div>
    <div class="author">
    	Theo {{ $arr->author }}
    </div>
</div>