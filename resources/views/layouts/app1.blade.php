<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0; border: 0">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        PHUC HOANG
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        <li><a href="{{ asset('/language/en') }}"><img src="{{ url('images/home/anh.png') }}" style="width: 44px;height: 28px "></a></li>
                        <li><a href="{{ asset('/language/vn') }}"><img src="{{ url('images/home/vn.gif') }}" style="width: 44px; height: 28px"></a></li>
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">{{ Lang::get('label.login') }}</a></li>
                            <li><a href="{{ url('/register') }}">{{ Lang::get('label.register') }}</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <!-- nav menu -->
        <nav class="navbar navbar-default menu" >
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">TRANG CHỦ</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="#">XÃ HỘI</a></li>
                        <li><a href="#">THẾ GIỚI</a></li>
                        <li><a href="#">KINH TẾ</a></li>
                        <li><a href="#">GIÁO DỤC</a></li>
                        <li><a href="#">THỂ THAO</a></li>
                        <li><a href="#">KHOA HỌC CÔNG NGHỆ</a></li>
                    </ul>
                </div>
        </nav>
        <!-- end nav menu -->
    </div>

    <!-- content -->
    <div class="container" style="margin-bottom: 30px; margin-top: 30px">
        <div class="row">
            <!-- content lefy -->
            <div class="col-md-8">
                @yield('left')
                @yield('comment')
            </div>
            <!-- end content left -->
            <!-- content right -->
            <div class="col-md-4" style="background: #EEEEEE;">
                <div style="font-size: 25px; color: black; font-weight: bold; padding-left: 20px; margin-bottom: 10px">TIN MỚI NHẤT</div>
                @yield('right')
            </div>
            <!-- end content right -->
        </div>
    </div>
    <!-- end content -->

    <!-- footer -->
    <div class="footer">
        <div class="container">
            Phuc Hoang <br>
            Công ty TNHH Công Nghệ Phần Mềm KAOPIZ
        </div>
    </div>
    <!-- end footer -->
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
