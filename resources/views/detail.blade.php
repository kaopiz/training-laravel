
@extends('layouts.app1')

@section('right')
    @foreach ($data_right as $row)
        @include('layouts.news-right',['arr'=>$row])
    @endforeach
@endsection

@section('left')
    @include('layouts.content',['arr'=>$data_left])
@endsection

@section('comment')
	@include('layouts.comment',['arr'=>$arr_comment])	
@endsection