
@extends('layouts.app1')

@section('right')
    @foreach ($data_right as $row)
        @include('layouts.news-right',['arr' => $row])
    @endforeach
@endsection

@section('left')
    @foreach ($data_left as $row)
        @include('layouts.box_new',['arr' => $row])
    @endforeach
    {{ $data_left->links() }}
@endsection