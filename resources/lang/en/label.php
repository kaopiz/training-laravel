<?php

return [
    'name' => 'Name',
    'laravel' => 'Laravel',
    'login' => 'Login',
    'register' => 'Register',
    'doc' => 'Documentation',
    'new' => 'News',
    'email' => 'E-Mail Address',
    'password' => 'Password',
    'comfirm_password' => 'Comfirm Password',
    'remember_me' => 'Remember Me',
    'forgot_password' => 'Forgot Your Password?',
    'lang' => [
        'en' => 'English',
        'vi' => 'Tiếng Việt',
        'jp' => '日本語',
    ],
];