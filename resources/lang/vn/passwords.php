<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Mật khẩu phải tối thiểu 6 ký tự và xác nhận đúng.',
    'reset' => 'Mật khẩu của bạn đã thiết lập lại!',
    'sent' => 'Đã có một email thiết lập mật khẩu gửi tới mail của bạn!',
    'token' => 'Thiết lập lại không hợp lệ.',
    'user' => "Chúng tôi không thể tìm thấy người dùng với email này.",

];
