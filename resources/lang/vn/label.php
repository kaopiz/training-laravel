<?php

return [
    'name' => 'Tên',
    'laravel' => 'Laravel',
    'login' => 'Đăng nhập',
    'register' => 'Đăng ký',
    'doc' => 'Tài liệu tham khảo',
    'new' => 'Tin tức',
    'email' => 'Địa chỉ email',
    'password' => 'Mật khẩu',
    'comfirm_password' => 'Nhập lại mật khẩu',
    'remember_me' => 'Ghi nhớ đăng nhập',
    'forgot_password' => 'Quên mật khẩu',
    'lang' => [
        'en' => 'English',
        'vi' => 'Tiếng Việt',
    ],
];